void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  char c;
  c=Serial.read();//不会等待输入结束才执行
  Serial.println(c);
  delay(5000);

}

/*
无输入默认，读得FF 0D 0A,可能在某些调试助手不显示，或者乱码

通过串口中断方式读入，一次可以读64字节

*/
