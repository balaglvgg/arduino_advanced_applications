/*
如果想进行串口中断的话，就编写一个serialEvent函数，定义在loop函数下面即可。

SerialEvent.ino为例子

*/

#include<Arduino.h>
int mian(void){
	init();
	#if defined(USBCON);
	USBDevice.attach();
	#endif
	setup();
	for (;;)
	{
		loop();
		if(serialEvenRun) serialEvenRun();

		/* code */
	}
	return 0;
}

